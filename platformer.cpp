#include "platformer.hpp"

#include <map>
#include <string>
#include <vector>

using namespace blit;

constexpr Size screen_size(160, 120);

Point world_to_screen(const Vec2 &p);
Point world_to_screen(const Point &p);
Point screen_to_world(const Point &p);
Point tile(const Point &p);
void draw_layer(MapLayer &layer);
void load_assets();


/* create map */
enum TileFlags { SOLID = 1, WATER = 2, LADDER = 4 };
Map map(Rect(0, 0, 48, 24));

struct Collision {
    bool top = false;
    bool right = false;
    bool bottom = false;
    bool left = false;
};

struct _Rect: Rect {
    _Rect(int32_t x, int32_t y, int32_t w, int32_t h): Rect(x, y, w, h) {}
    [[nodiscard]] int32_t bottom() const { return y + h; }
    [[nodiscard]] int32_t top() const { return y; }
    [[nodiscard]] int32_t left() const { return x; }
    [[nodiscard]] int32_t right() const { return x + w; }
};

class PhysicalObject {
public:
    Vec2 vel;  // velocity
    Size size;
private:
    Point pos = Point();  // position (used for representation)
    Vec2 fpos = Vec2();  // floating point position (used for internal calculation)

public:
    PhysicalObject(Vec2 pos, Vec2 vel, Size size) {
        set_pos(pos);
        this->vel = vel;
        this->size = size;
    }

    [[nodiscard]] virtual const Point & get_pos() const final {
        return pos;
    }

    /**
     * @param position to be set where the physical object should reside
     */
    virtual void set_pos(Vec2 position) final {
        set_x_pos(position.x);
        set_y_pos(position.y);
    }

    virtual void set_x_pos(float x_position) final {
        fpos.x = x_position;
        pos.x = static_cast<int32_t>(std::nearbyint(x_position));
    }

    virtual void set_y_pos(float y_position) final {
        fpos.y = y_position;
        pos.y = static_cast<int32_t>(std::nearbyint(y_position));
    }

    /**
     * Resolve collisions between entity and map
     */
    virtual void resolve_map_collisions() final {
        auto collision = Collision();
        // Handle Left/Right collisions
        set_x_pos(fpos.x + vel.x);
        _Rect player = bounds();

        map.tiles_in_rect(player, [player, &collision, this](Point tile_pt) -> void {
            if (map.has_flag(tile_pt, TileFlags::SOLID)) {
                _Rect tile(tile_pt.x * 8, tile_pt.y * 8, 7, 7);
                // check if the tile is right or left of the player
                // by checking if both are overlapping (either the top side or the bottom side of the tile is inside the
                // players bounds).
                // BEWARE: This only works since a tile is always at most as large as the player.
                //         This does not work if a tile can be larger since then the tile bottom and top can both be
                //         outside the players bounds while still colliding (if the tiles bounds are fully included
                //         inside the players bounds).
                //         A general but less efficient solution would be: (min(right_t, right_p) > max(left_t, left_p))
                if(((player.top() <= tile.bottom()) && (tile.bottom() <= player.bottom()))
                   || ((player.top() <= tile.top()) && tile.top() <= player.bottom())){
                    // Collide the left-hand side of the tile right of player
                    if(player.right() >= tile.left() && (player.left() <= tile.left())){
                        set_x_pos(float(tile.left() - 1 - player.w));
                        vel.x = 0.0f;
                        collision.right = true;
                    }
                    // Collide the right-hand side of the tile left of player
                    if((player.left() <= tile.right()) && (player.right() >= tile.right())) {
                        set_x_pos(float(tile.right() + 1));
                        vel.x = 0.0f;
                        collision.left = true;
                    }
                }
            }
        });

        // Handle Up/Down collisions
        set_y_pos(fpos.y + vel.y);
        // update player bounds
        player = bounds();

        map.tiles_in_rect(player, [player, &collision, this](Point tile_pt) -> void {
            if (map.has_flag(tile_pt, TileFlags::SOLID)) {
                _Rect tile(tile_pt.x * 8, tile_pt.y * 8, 7, 7);
                // check if the tile is above or below the player
                // BEWARE: This only works because the player is not wider than the tile (in horizontal direction!).
                //         Otherwise, the players bounds might fully include the tile which leads to the same problem as
                //         above.
                if((player.right() >= tile.left()) && (player.left() <= tile.right())){
                    // Collide bottom side of tile above player
                    if(player.top() <= tile.bottom() && player.bottom() >= tile.bottom()){
                        set_y_pos(float(tile.bottom() + 1 + player.h));
                        vel.y = 0.f;
                        collision.top = true;
                    }
                    // Collide the top side of the tile below player
                    if((player.bottom() >= tile.top()) && (player.top() <= tile.top())){
                        set_y_pos(tile.top() - 1);
                        vel.y = 0.f;
                        collision.bottom = true;
                    }
                }
            }
        });

        on_collision(collision);
    }

    virtual void on_collision(Collision & collision) = 0;

    virtual ~PhysicalObject() = default;

private:

    [[nodiscard]] _Rect bounds() const {
        return {static_cast<int32_t>(pos.x), static_cast<int32_t>(pos.y - size.h), size.w, size.h};
    }
};

struct Player : PhysicalObject {
  bool flip = false;

  enum State {
    IDLE,
    WALKING,
    JUMPING,
    SWIMMING,
    CLIMBING
  };

  State state = IDLE;

  std::map<uint8_t, std::vector<uint8_t>> animations;
  uint32_t animation_frame = 0;

  Player() : PhysicalObject(Vec2(100, 32), Vec2(0, 0), Size(7, 14)) {
    animations[IDLE] = {208, 208, 208, 208, 208, 208, 209, 208, 208, 208, 208, 208, 208, 208 };
    animations[WALKING] = { 208, 209, 210, 211, 212 };
    animations[JUMPING] = { 217 };
    animations[SWIMMING] = { 217 };
    animations[CLIMBING] = { 217 };
  }

    void on_collision(Collision & collision) override {
      if (collision.bottom) {
          state = IDLE;
      }
  }

  uint8_t animation_sprite_index(uint8_t animation) {
    auto animation_length = animations[animation].size();
    return animations[animation][animation_frame % animation_length];
  }

  [[nodiscard]] Point current_tile() const {
    Point pos = get_pos();
    return Point((pos.x + (size.w / 2)) / 8, (pos.y - 8) / 8);
  }

  [[nodiscard]] bool tile_under_solid() const {
    Point p = current_tile();
    return map.has_flag(p, TileFlags::SOLID);
  }

  [[nodiscard]] bool tile_under_ladder() const {
    Point p = current_tile();
    return map.has_flag(p, TileFlags::LADDER);
  }

  [[nodiscard]] bool tile_under_water() const {
    Point p = current_tile();
    return map.has_flag(p, TileFlags::WATER);
  }

  [[nodiscard]] bool on_ground() const {
    return state == IDLE || state == WALKING || state == SWIMMING || state == CLIMBING;
    /*if (vel.y < 0) return false;

    Point p = current_tile();
    return map.has_flag(Point(p.x, p.y + 1), TileFlags::SOLID) && ((int32_t(pos.y) % 8) == 0);*/
  }

  [[nodiscard]] bool in_water() const {
    return false;
    Point p = current_tile();
    return map.has_flag(Point(p.x, p.y + 1), TileFlags::WATER);
  }

  /*
    return a clipped camera point that doesn't allow the viewport to
    leave the world bounds
  */
  [[nodiscard]] Point camera() const {
    static const Rect b(screen_size.w / 2, screen_size.h / 2, map.bounds.w * 8 - screen.bounds.w, map.bounds.h * 8 - screen.bounds.h);
    return b.clamp(get_pos());
  }

  [[nodiscard]] Rect viewport() const {
    Point c = camera();
    return Rect(
      c.x - screen.bounds.w / 2,
      c.y - screen.bounds.h / 2,
      screen.bounds.w,
      screen.bounds.h
    );
  }

  void reset_animation() {
      animation_frame = 0;
  }

  void update_animation() {
      animation_frame++;
  }

  void update() {
    static float ground_acceleration_x = 0.5f;
    static float air_acceleration_x = 0.2f;
    static float ground_drag_x = 0.70f;
    static float air_drag_x = 0.8f;
    static float jump_velocity = 4.0f;
    static Vec2 gravity(0, 0.98f / 10.0f);

    bool is_on_ground = on_ground();

    State previous_state = state;

    vel.x *= is_on_ground ? ground_drag_x : air_drag_x;
    if(!tile_under_ladder()) vel += gravity;
    if(tile_under_water()) {
      vel.y *= 0.80f;
    }
    else {
      vel.y *= tile_under_ladder() ? 0.80f : 0.95f;
    }
    resolve_map_collisions();

    if (buttons.state & Button::DPAD_LEFT) {
      vel.x -= is_on_ground ? ground_acceleration_x : air_acceleration_x;
      flip = true;
      if(is_on_ground) {
        state = WALKING;
      }
    }

    if (buttons.state & Button::DPAD_RIGHT) {
      vel.x += is_on_ground ? ground_acceleration_x : air_acceleration_x;
      flip = false;
      if(is_on_ground) {
        state = WALKING;
      }
    }

    if (is_on_ground && buttons.pressed & Button::A) {
      vel.y += -jump_velocity;
      state = JUMPING;
    }

    if(tile_under_ladder() || tile_under_water()) {
      if (buttons.state & Button::DPAD_UP) {
        vel.y -= 0.2f;
      }
      if (buttons.state & Button::DPAD_DOWN) {
        vel.y += 0.2f;
      }
      state = tile_under_water() ? SWIMMING : CLIMBING;
    }

      // resets the animation if the state changes so that animations always start at the beginning
      // which looks smoother
      if (state != previous_state) {
          reset_animation();
      }
  }

  void render() {
    uint8_t animation = Player::IDLE;

    if (std::abs(vel.x) > 1) {
      animation = Player::WALKING;
    }
    if (!on_ground()) {
      animation = Player::JUMPING;
    }

    uint8_t si = animation_sprite_index(animation);

    Point p = get_pos();
    p.y -= 8;
    Point sp = world_to_screen(p);
    screen.sprite(si, sp, flip);
    sp.y -= 8;
    screen.sprite(si - 16, sp, flip);
#ifdef COLLISION_DEBUG
    Rect bb = bounds();
    screen.pen = Pen(0, 255, 0);
    screen.line(world_to_screen(bb.tl()), world_to_screen(bb.tr()));
    screen.line(world_to_screen(bb.bl()), world_to_screen(bb.br()));

    // Collisions Debug
    map.tiles_in_rect(bb, [&bb](Point tile_pt) -> void {
      Point sp = world_to_screen(tile_pt * 8);
      Rect rb(sp.x, sp.y, 8, 8);

      screen.pen = Pen(0, 255, 0, 150);
      if (map.has_flag(tile_pt, TileFlags::SOLID)) {
        screen.pen = Pen(255, 0, 0, 150);
      }

      screen.rectangle(rb);
    });
#endif
  }
} player;


struct bat {
  Vec2 pos;
  Vec2 vel = Vec2(-2, 0);
  uint8_t current_frame = 0;
  std::array<uint8_t, 6> frames = {{ 96, 97, 98, 99, 98, 97 }};

  void update() {
    current_frame++;
    current_frame %= frames.size();

    Vec2 test_pos = pos + (Vec2::normalize(vel) * 8.0f);

    if (map.has_flag(tile(Point(test_pos)), TileFlags::SOLID)) {
      vel.x *= -1;
    }

    pos += vel;
  }
};

bat bat1;

struct slime {
  Vec2 pos;
  Vec2 vel = Vec2(1, 0);
  uint8_t current_frame = 0;
  std::array<uint8_t, 4> frames = {{ 112, 113, 114, 113 }};

  void update() {
    current_frame++;
    current_frame %= frames.size();

    Vec2 test_pos = pos + (Vec2::normalize(vel) * 8.0f);

    if (map.has_flag(tile(Point(test_pos)), TileFlags::SOLID)) {
      vel.x *= -1;
    }

    pos += vel;
  }
};

slime slime1;

void animation_timer_callback(Timer &timer) {
  bat1.update();
  slime1.update();
    player.update_animation();
}

Timer t;






/* setup */
void init() {
    set_screen_mode(ScreenMode::lores);

  load_assets();

  bat1.pos = Vec2(200, 22);
  slime1.pos = Vec2(50, 112);

  t.init(animation_timer_callback, 50, -1);
  t.start();
}


#ifdef PLAYER_MAP_DEBUG
void highlight_tile(Point p, Pen c) {
    screen.pen = c;
    p.x *= 8;
    p.y *= 8;
    p = world_to_screen(p);
    screen.rectangle(Rect(p.x, p.y, 8, 8));
}
#endif


void render(uint32_t time) {
  uint32_t ms_start = now();

  screen.mask = nullptr;
  screen.alpha = 255;
  screen.pen = Pen(0, 0, 0);
  screen.clear();


  // draw world
  // layers: background, environment, effects, characters, objects
  draw_layer(map.layers["background"]);
  draw_layer(map.layers["environment"]);
  draw_layer(map.layers["effects"]);
  draw_layer(map.layers["objects"]);


  // draw player
  player.render();


    // bat
    Point sp = world_to_screen(Point(bat1.pos.x - 4, bat1.pos.y));
    screen.sprite(bat1.frames[bat1.current_frame], sp, bat1.vel.x >= 0);

    // slime
    sp = world_to_screen(Point(slime1.pos.x - 4, slime1.pos.y));
    screen.sprite(slime1.frames[slime1.current_frame], sp, slime1.vel.x >= 0);


  // overlay water
  screen.pen = Pen(56, 136, 205, 125);
  for (uint8_t y = 0; y < 24; y++) {
    for (uint8_t x = 0; x < 48; x++) {
      Point pt = world_to_screen(Point(x *   8, y * 8));

      if (map.has_flag(Point(x, y), TileFlags::WATER)) {
        screen.rectangle(Rect(pt.x, pt.y, 8, 8));
      }
    }
  }

  screen.mask = nullptr;
  screen.alpha = 255;
  screen.sprite(139, Point(2, 2));
  screen.sprite(139, Point(12, 2));
  screen.sprite(139, Point(22, 2));


  // draw FPS meter
  uint32_t ms_end = now();
  screen.mask = nullptr;
  screen.pen = Pen(255, 0, 0);
  for (uint32_t i = 0; i < (ms_end - ms_start); i++) {
    screen.pen = Pen(i * 5, 255 - (i * 5), 0);
    screen.rectangle(Rect(i * 3 + 1, 117, 2, 2));
  }

#ifdef PLAYER_MAP_DEBUG
    // highlight current player tile
    Point pt2 = player.current_tile();
    highlight_tile(pt2, Pen(0, 255, 0, 100));

    // draw map flags
    constexpr Pen flag_colours[] = {
        Pen(255, 0, 0, 100),
        Pen(0, 255, 0, 100),
        Pen(0, 0, 255, 100)
    };
    for (uint8_t y = 0; y < 24; y++) {
        for (uint8_t x = 0; x < 48; x++) {
            Point pt = world_to_screen(Point(x * 8, y * 8));
            uint8_t f = map.get_flags(Point(x, y));
            for (uint8_t i = 0; i < 3; i++) {
                if (f & (1 << i)) {
                    screen.pen = flag_colours[i];
                    screen.rectangle(Rect(pt.x, pt.y, 8, 8));
                }
            }
        }
  }
#endif
}


/*
  update() is called every 10ms, all effects should be
  scaled to that duration

  player velocity is in tiles per second, so if the players
  'x' velocity is 1 then they move sideways by one tile per
  second

  one tile is considered to be 1 metre
*/
void update(uint32_t time) {
  player.update();
}


void load_assets() {
  std::vector<uint8_t> layer_background = { 17,17,17,17,17,17,17,17,17,17,17,17,17,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,17,17,17,17,17,17,17,17,17,17,17,17,17,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,17,17,17,17,17,17,17,17,17,17,17,17,17,17,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,47,1,2,3,4,1,2,3,1,2,3,4,5,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,0,0,0,0,0,0,0,51,0,0,0,13,14,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,68,47,0,0,0,30,0,0,0,0,15,0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,31,84,47,0,0,0,0,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,31,67,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,22,0,0,0,0,0,0,0,0,0,23,0,0,0,0,0,0,0,0,0,0,0,0,0,0,31,83,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,223,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,0,41,0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,15,0,78,0,0,0,0,0,0,0,0,15,0,0,78,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15,15,15,15,15,41,15,15,15,41,15,41,15,15,15,41,0,0,0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,41,15,41,15,15,15,15,41,15,15,15,15,15,60,15,15,0,0,0,15,41,41,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15,60,15,15,15,15,15,41,15,15,41,15,15,15,41,15,41,41,15,15,15,15,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,15,15,15,41,15,13,15,15,41,15,15,41,15,41,15,15,15,15,41,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15,41,15,15,41,15,15,15,41,41,15,15,15,15,15,30,15,15,15,41,15,60,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,15,15,15,15,15,41,15,15,15,60,15,15,41,15,15,15,15,41,15,41,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };;
  map.add_layer("background", layer_background);

  std::vector<uint8_t> layer_environment = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,0,74,28,29,60,0,0,0,0,0,0,60,28,29,0,0,15,0,0,0,0,0,0,0,0,0,0,28,29,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,0,74,44,45,0,0,0,0,0,0,0,0,44,45,0,0,0,0,15,0,0,0,0,15,0,0,0,44,45,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,0,74,0,0,0,0,0,0,0,0,0,0,60,89,89,89,89,71,0,0,0,0,0,0,0,0,0,74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,89,90,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,28,29,0,0,0,0,0,15,0,74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,190,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,44,45,0,0,15,0,0,0,0,74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,121,28,29,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,71,0,0,0,0,0,0,74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,44,45,58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,89,89,89,89,89,71,74,0,0,0,0,0,0,0,0,0,0,0,60,57,57,87,0,0,74,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,0,0,0,0,0,0,0,0,60,74,0,0,0,56,57,0,0,0,0,56,57,87,0,0,0,0,0,86,57,57,57,57,28,29,57,60,57,57,57,50,55,55,55,55,48,57,57,57,60,0,0,0,0,0,0,0,28,29,55,55,55,64,0,58,7,57,60,72,0,0,0,0,75,94,94,94,94,94,94,76,44,45,0,0,0,0,0,66,16,16,48,49,127,0,0,28,29,60,0,0,0,0,0,0,44,45,16,16,16,64,0,74,7,0,0,72,0,0,0,0,79,0,0,0,0,0,0,77,0,75,94,94,94,94,76,126,49,49,127,0,0,0,0,44,45,66,55,55,55,55,55,55,60,16,16,16,16,64,0,74,7,0,0,93,94,94,94,94,95,0,0,0,0,0,0,93,94,95,0,0,0,0,47,0,0,0,0,0,0,0,0,0,0,66,16,16,16,16,16,16,16,16,16,16,16,64,0,74,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,0,0,0,0,0,0,0,0,0,0,126,49,49,49,50,16,16,16,16,16,16,48,127,0,31,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,66,16,16,16,16,16,48,127,0,0,31,7,0,0,0,0,0,0,0,0,40,61,62,62,63,0,0,0,0,0,0,0,0,47,0,0,0,0,0,0,0,0,0,0,0,0,0,0,126,49,49,49,49,49,127,0,0,0,91,62,62,62,62,62,62,62,62,62,62,92,0,0,91,62,62,63,0,61,62,62,62,92,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,79,0,77,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,79,59,77,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  map.add_layer("environment", layer_environment);
  map.layers["environment"].add_flags({ 8, 59, 31, 47, 28, 29, 44, 45, 60, 48, 49, 50, 64, 66, 80, 81, 82, 56, 57, 58, 72, 74, 88, 89, 90, 61, 62, 63, 77, 79, 93, 94, 95 }, TileFlags::SOLID);
  map.layers["environment"].add_flags(7, TileFlags::LADDER);
  map.layers["environment"].add_flags({ 16, 55, 223 }, TileFlags::WATER);

  std::vector<uint8_t> layer_effects = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,33,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,37,0,0,37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,52,52,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  map.add_layer("effects", layer_effects);

  std::vector<uint8_t> layer_characters = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,0,0,0,107,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,208,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,166,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,122,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  map.add_layer("characters", layer_characters);

  std::vector<uint8_t> layer_objects = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,51,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,84,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,39,0,0,0,0,0,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,39,0,0,0,0,0,0,0,0,0,0,85,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,84,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  map.add_layer("objects", layer_objects);

  screen.sprites = Surface::load(packed_data);
}


Point world_to_screen(const Vec2 &p) {
  return {
    static_cast<int32_t>(p.x - player.camera().x + screen_size.w / 2),
    static_cast<int32_t>(p.y - player.camera().y + screen_size.h / 2)
  };
}

Point world_to_screen(const Point &p) {
  return {
    p.x - player.camera().x + screen_size.w / 2,
    p.y - player.camera().y + screen_size.h / 2
  };
}

Point screen_to_world(const Point &p) {
  return {
    p.x + player.camera().x - screen_size.w / 2,
    p.y + player.camera().y - screen_size.h / 2
  };
}


Point tile(const Point &p) {
  return Point(p.x / 8, p.y / 8);
}


void draw_layer(MapLayer &layer) {
    Point topLeft = screen_to_world(Point(0, 0));
    Point bottomRight = screen_to_world(Point(screen.bounds.w, screen.bounds.h));

    Point topLeftTile = tile(topLeft);
    Point bottomRightTile = tile(bottomRight);

    for (uint8_t y = topLeftTile.y; y <= bottomRightTile.y; y++) {
        for (uint8_t x = topLeftTile.x; x <= bottomRightTile.x; x++) {
            Point pt = world_to_screen(Point(x * 8, y * 8));
            int32_t ti = layer.map->tile_index(Point(x, y));
            if (ti != -1) {
                uint8_t si = layer.tiles[ti];
                if (si != 0) {
                    screen.sprite(si, pt);
                }
            }
        }
    }
}
